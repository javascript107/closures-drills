function counterFactory() {
  // Return an object that has two methods called `increment` and `decrement`.
  // `increment` should increment a counter variable in closure scope and return it.
  // `decrement` should decrement the counter variable and return it.
  return {
    increment(a) {
      return (a += 1);
    },
    decrement(a) {
      return (a -= 1);
    }
  };
}

module.exports = counterFactory;
