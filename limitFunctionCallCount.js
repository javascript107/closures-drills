function limitFunctionCallCount(cb, n) {
  let count = 0;
  if (cb === undefined && n === undefined) {
    throw new Error(
      `Please pass all the arguments to the function "limitFunctionCallCount"`
    );
  } else if (cb !== undefined && cb.constructor.name !== "Function") {
    throw new Error(`Please pass name of a callback function as a value to cb`);
  } else if (n === undefined) {
    throw new Error("n is not defined");
  } else if (n !== undefined && typeof n !== "number") {
    throw new Error("Please pass valid argument as a value to n");
  } else {
    function invokecb(...args) {
      if (count < n) {
        count += 1;
        if (cb(...args) !== undefined) {
          return cb(...args);
        } else {
          return null;
        }
      }
      return null;
    }
  }
  return invokecb;
}

module.exports = limitFunctionCallCount;
