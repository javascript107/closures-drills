const cacheFunction = require("../cacheFunction")
const sumofThree = (a,b,c) => a + b + c;

try{
    const result = cacheFunction(sumofThree)
    console.log(result(1,2,3))
    console.log(result(1,2,3))
    console.log(result(1,2,6))
    console.log(result(1,2,6))
    }
    catch(error){
        console.log(error)
    }
    