function cacheFunction(cb) {
  let cache = { argumentsUsed: [] };
  if (cb === undefined) {
    throw new Error(`cb is not defined`);
  } else if (cb !== undefined && cb.constructor.name !== "Function") {
    throw new Error(
      "Please pass valid callback function as an argument to cache function"
    );
  } else {
    function invokecb(...args) {
      let isArgumentsUsed = false;
      for (let index = 0; index < cache.argumentsUsed.length; index++) {
        if (
          JSON.stringify(cache.argumentsUsed[index]) === JSON.stringify(args)
        ) {
          isArgumentsUsed = true;
          return cache;
        }
      }
      if (!isArgumentsUsed) {
        cache.argumentsUsed.push(args);
        return cb(...args);
      }
    }
    return invokecb;
  }
}

module.exports = cacheFunction;
